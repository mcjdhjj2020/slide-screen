package com.example;

import java.io.File;

public class Constants {
    public static String ADB_PATH;

    public static String getAdbPath(String path) {
        if (ADB_PATH == null) {
            if (path == null || path.isEmpty()) {
                ADB_PATH = new File("resource\\adb").getAbsolutePath();
            } else {
                File BASE_DIR = new File(path);
                ADB_PATH = new File(BASE_DIR, "resource\\adb").getAbsolutePath();
            }
        }
        return ADB_PATH;
    }
}
