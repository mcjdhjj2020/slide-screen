package com.example;

import com.example.util.HelperUtil;

import java.util.Random;

public class Helper {

    public void start(String path) {
        Random random = new Random();
        while (true) {
            try {
                HelperUtil.execute(Constants.getAdbPath(path), "shell", "input", "swipe", "400", "100", "200", "100");
                int time = random.nextInt(10) + 30;
                System.out.println("tap 指令点击: " + time);
                Thread.sleep(time * 1000);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
