package com.example.util;

public class HelperUtil {

    /**
     * 执行Shell命令，同步耗时操作
     */
    public static final boolean execute(String... cmd) {
        try {
            Process process = Runtime.getRuntime().exec(cmd);
            int result = process.waitFor();
            if (result != 0) {
                System.out.println("Failed to execute \"" + cmd + "\", result code is " + result);
            } else {
                return true;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }
}
